from behave import given, when, then
from calculator import exercise2, exercise22, integralcalculator, calculatorModule


@given(u'Exercise 2 submodule is run')
def step_impl(context):
    print(u'STEP: Given Exercise 2 submodule is run')
    pass


@when(u'I input "{}" and "{}" and "{}" and "{}"')
def step_impl(context, a, powa, b, powb):
    print(u'STEP: When I input "{}" and "{}" and "{}" and "{}"'.format(a, powa, b, powb))
    context.result = len(str(exercise2(a, powa, b, powb)))


@then(u'I get result "{out}"')
def step_impl(context, out):
    print(u'STEP Then I get result "{}"'.format(out))
    assert context.result == int(out), 'Expected {}, got {}'.format(out, context.result)


@given(u'Exercise 22 submodule is run')
def step_impl(context):
    print(u'STEP: Given Exercise 22 submodule is run')
    pass


@when(u'I input "{}"')
def step_impl(context, r):
    print(u'STEP: When I input "{}" '.format(r))
    context.result = int(exercise22(r))


@given(u'Integration submodule is run')
def step_impl(context):
    print(u'STEP: Integration submodule is run')
    pass


@when(u'I input "{}" and "{}" to IntegralCalculator')
def step_impl(context, func, dx):
    print(u'STEP: When I input "{}" and "{}" '.format(func, dx))
    context.result = integralcalculator(func, dx)


@then(u'I get result "{out}" for IntegralCalculator')
def step_impl(context, out):
    print(u'STEP Then I get result "{}" for IntegralCalculator'.format(out))
    assert context.result == out, 'Expected {}, got {}'.format(out, context.result)


@given(u'Calculator submodule is run')
def step_impl(context):
    print(u'STEP: Given Calculator submodule is run')
    pass


@when(u'I input "{}" to Calculator submodule')
def step_impl(context, inp):
    print(u'STEP: When I input {} to Calculator submodule'.format(calculatorModule(inp)))
    context.result = calculatorModule(inp)


@then(u'I get result "{out}" for Calculator submodule')
def step_impl(context, out):
    print(u'STEP Then I get result "{}"'.format(out))
    assert context.result == int(out), 'Expected {}, got {}'.format(out, context.result)
